package hu.dpc.edu.state;

import hu.dpc.edu.Calculator;
import hu.dpc.edu.CalculatorState;
import hu.dpc.edu.Operator;

public class EnteringFirstNumberState extends CalculatorState {

    @Override
    public void operatorPressed(Calculator calculator, Operator operator) {
        calculator.setOperator(operator);
        calculator.storeDisplayedTextAsFirstOperand();
        calculator.setState(new EnteringSecondNumberInitialState());
    }

    @Override
    public void numberButtonPressed(Calculator calculator, String num) {
        calculator.appendToDisplayedText(num);
    }

    @Override
    public void calculateButtonPressed(Calculator calculator) {
        //do nothing
    }

    @Override
    public void decimalSeparatorPressed(Calculator calculator) {
        calculator.appendToDisplayedText(".");
        calculator.setState(new EnteringFirstNumberDecimalPartState());
    }

    @Override
    public String toString() {
        return "Entering first number";
    }
}
