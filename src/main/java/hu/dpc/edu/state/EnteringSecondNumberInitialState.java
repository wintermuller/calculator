package hu.dpc.edu.state;

import hu.dpc.edu.Calculator;
import hu.dpc.edu.CalculatorState;
import hu.dpc.edu.Operator;

public class EnteringSecondNumberInitialState extends CalculatorState {

    @Override
    public void onEnter(Calculator calculator) {
        //do nothing
    }

    public void operatorPressed(Calculator calculator, Operator operator) {
        // replacing operator
        calculator.setOperator(operator);
    }

    @Override
    public void calculateButtonPressed(Calculator calculator) {
        //do nothing
    }

    @Override
    public void numberButtonPressed(Calculator calculator, String num) {
        calculator.setDisplayedText(num);
        calculator.setState(new EnteringSecondNumberState());
    }

    @Override
    public void decimalSeparatorPressed(Calculator calculator) {
        calculator.setDisplayedText("0.");
        calculator.setState(new EnteringSecondNumberDecimalPartState());
    }

    @Override
    public String toString() {
        return "Entering second number";
    }
}
