package hu.dpc.edu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

    private Calculator calculator;

    @BeforeEach
    void beforeEach() {
        calculator = new Calculator();
    }

    @Nested
    @DisplayName("Initially")
    class Initially {
        @Test
        @DisplayName("Initially 0 should be displayed")
        public void initialState() {
            assertEquals("0", calculator.getDisplayedText());
        }

        @Test
        @DisplayName("When pressing a number, it should replace the 0")
        public void pressingNumber() {
            calculator.numberButtonPressed(3);
            assertEquals("3", calculator.getDisplayedText());
        }

        @Test
        @DisplayName("When pressing the decimal separator, it should display '0.'")
        public void decimalSeparatorInitially() {
            calculator.decimalSeparatorPressed();
            assertEquals("0.", calculator.getDisplayedText());
        }

    }

    @Nested
    @DisplayName("During entering the first number")
    class FirstNumber {
        @BeforeEach
        void beforeEach() {
            calculator.numberButtonPressed(2);
            calculator.numberButtonPressed(9);
        }

        @Test
        @DisplayName("When decimal separator pressed it should be simply appended")
        public void pressingSecondNumber() {
            calculator.decimalSeparatorPressed();
            assertEquals("29.", calculator.getDisplayedText());
        }

        @Nested
        @DisplayName("After decimal sign was entered")
        class FirstNumberDecimal {
            @BeforeEach
            void beforeEach() {
                calculator.decimalSeparatorPressed();
            }

            @Test
            @DisplayName("A second decimal separator does nothing")
            public void pressingSecondNumber() {
                calculator.decimalSeparatorPressed();
                assertEquals("29.", calculator.getDisplayedText());
            }

            @Test
            @DisplayName("A second decimal separator does nothing even after more numbers")
            public void pressingSecondNumberAfterNumbers() {
                calculator.numberButtonPressed(7);
                calculator.numberButtonPressed(6);
                calculator.decimalSeparatorPressed();
                assertEquals("29.76", calculator.getDisplayedText());
            }

        }
    }

    @Nested
    @DisplayName("After first integer is entered")
    class AfterFirstNumber {
        @BeforeEach
        void beforeEach() {
            calculator.numberButtonPressed(3);
            calculator.numberButtonPressed(4);
        }

        @Nested
        @DisplayName("And Add button is pressed")
        class AddButtonPressed {
            @BeforeEach
            void beforeEach() {
                calculator.addButtonPressed();
            }

            @Test
            @DisplayName("displayedText remains unchanged")
            public void pressingSecondNumber() {
                assertEquals("34", calculator.getDisplayedText());
            }

            @Test
            @DisplayName("Pressing a number button replaced the displayedText")
            public void pressingSecondNumberAfterNumbers() {
                calculator.numberButtonPressed(7);
                assertEquals("7", calculator.getDisplayedText());
            }

            @Test
            @DisplayName("Pressing a second number button appends the displayedText")
            public void pressingMultipleNumbersAppendsDisplayedText() {
                calculator.numberButtonPressed(7);
                calculator.numberButtonPressed(3);
                assertEquals("73", calculator.getDisplayedText());
            }


        }


    }

    @Nested
    @DisplayName("After multiple numbers have been entered")
    class AfterMultipleNumbers {

        @BeforeEach
        void beforeEach() {
            calculator.numberButtonPressed(3);
            calculator.numberButtonPressed(4);
            calculator.addButtonPressed();
            calculator.numberButtonPressed(2);
            calculator.numberButtonPressed(1);
        }

        @Test
        @DisplayName("Pressing an operator button shows the current result")
        public void pressingOperatorButtonAfterMultipleOperands() {
            calculator.addButtonPressed();
            assertEquals("55.0", calculator.getDisplayedText());
        }

        @Test
        @DisplayName("Pressing the calculate button shows the current result")
        public void pressingCalculateButtonAfterMultipleOperands() {
            calculator.calculateButtonPressed();
            assertEquals("55.0", calculator.getDisplayedText());
        }



    }

    @Nested
    @DisplayName("After pressing the calculate button")
    class AfterCalculateButton {

        @BeforeEach
        void beforeEach() {
            calculator.numberButtonPressed(1);
            calculator.numberButtonPressed(6);
            calculator.addButtonPressed();
            calculator.numberButtonPressed(3);
            calculator.numberButtonPressed(2);
            calculator.calculateButtonPressed();
        }

        @Test
        @DisplayName("Pressing the calculate button shows the current result")
        public void pressingCalculateButtonAfterMultipleOperands() {
            assertEquals("48.0", calculator.getDisplayedText());
        }

        @Test
        @DisplayName("Pressing a number button replaces the current result")
        public void pressingNumberButtonAfterCalculateButton() {
            calculator.numberButtonPressed(4);
            assertEquals("4", calculator.getDisplayedText());
        }

        @Test
        @DisplayName("Pressing an operator button makes current result the first operand")
        public void pressingOperatorButtonAfterCalculateButton() {
            calculator.addButtonPressed();
            assertEquals("48.0", calculator.getDisplayedText());
            calculator.numberButtonPressed(6);
            calculator.calculateButtonPressed();
            assertEquals("54.0", calculator.getDisplayedText());
        }

        @Test
        @DisplayName("Pressing the calculator button again does nothing")
        public void pressingCalculatorButtonAfterCalculateButton() {
            assertEquals("48.0", calculator.getDisplayedText());
        }


    }

    @Nested
    @DisplayName("Testing the four basic operations")
    class BasicOperations {

        @BeforeEach
        void beforeEach() {
            calculator.numberButtonPressed(1);
            calculator.numberButtonPressed(6);
        }

        @Test
        @DisplayName("Addition")
        public void testAddition() {
            calculator.addButtonPressed();
            calculator.numberButtonPressed(5);
            calculator.numberButtonPressed(1);
            calculator.calculateButtonPressed();
            assertEquals("67.0", calculator.getDisplayedText());
        }

        @Test
        @DisplayName("Subtraction")
        public void testSubtraction() {
            calculator.subtractButtonPressed();
            calculator.numberButtonPressed(9);
            calculator.calculateButtonPressed();
            assertEquals("7.0", calculator.getDisplayedText());
        }

        @Test
        @DisplayName("Multiplication")
        public void testMultiplication() {
            calculator.multiplyButtonPressed();
            calculator.numberButtonPressed(4);
            calculator.calculateButtonPressed();
            assertEquals("64.0", calculator.getDisplayedText());
        }

        @Test
        @DisplayName("Division")
        public void testDivision() {
            calculator.divisionButtonPressed();
            calculator.numberButtonPressed(4);
            calculator.calculateButtonPressed();
            assertEquals("4.0", calculator.getDisplayedText());
        }

    }

}