package hu.dpc.edu.state;

import hu.dpc.edu.Calculator;
import hu.dpc.edu.CalculatorState;
import hu.dpc.edu.Operator;

public class DisplayResultState extends CalculatorState {
    @Override
    public void operatorPressed(Calculator calculator, Operator operator) {
        calculator.setOperator(operator);
        calculator.storeDisplayedTextAsFirstOperand();
        calculator.setState(new EnteringSecondNumberInitialState());
    }

    @Override
    protected void numberButtonPressed(Calculator calculator, String number) {
        calculator.setDisplayedText(number);
        calculator.setState(new EnteringFirstNumberState());
    }

    @Override
    public void calculateButtonPressed(Calculator calculator) {
        // do nothing
    }

    @Override
    public void decimalSeparatorPressed(Calculator calculator) {
        calculator.setDisplayedText("0.");
        calculator.setState(new EnteringFirstNumberDecimalPartState());
    }

    @Override
    public String toString() {
        return "Showing result";
    }

}
