package hu.dpc.edu;

import hu.dpc.edu.state.InitialState;

public class Calculator {

    private CalculatorState state;

    private String displayedText;

    private double firstOperand;

    private Operator operator;

    public Calculator() {
        setState(new InitialState());
    }

    public void appendToDisplayedText(String string) {
        displayedText = displayedText + string;
    }

    public void setState(CalculatorState state) {
        if (this.state != null) {
            this.state.onLeave(this);
        }
        this.state = state;
        this.state.onEnter(this);
    }

    public void setDisplayedText(String displayedText) {
        this.displayedText = displayedText;
    }

    public double getFirstOperand() {
        return firstOperand;
    }

    public void setFirstOperand(double firstOperand) {
        this.firstOperand = firstOperand;
    }

    public Operator getOperator() {
        return operator;
    }

    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    public void addButtonPressed() {
        state.addButtonPressed(this);
    }

    public void subtractButtonPressed() {
        state.subtractButtonPressed(this);
    }

    public void multiplyButtonPressed() {
        state.multiplyButtonPressed(this);
    }

    public void divisionButtonPressed() {
        state.divisionButtonPressed(this);
    }

    public void numberButtonPressed(int num) {
        if (num < 0 || num > 9) {
            throw new IllegalArgumentException("Number is out of range");
        }
        state.numberButtonPressed(this, num);
    }

    public void decimalSeparatorPressed() {
        state.decimalSeparatorPressed(this);
    }

    public void calculateButtonPressed() {
        state.calculateButtonPressed(this);
    }

    private double getDisplayedTextAsDouble() {
        return Double.parseDouble(displayedText);
    }

    public void storeDisplayedTextAsFirstOperand() {
        this.firstOperand = getDisplayedTextAsDouble();
    }

    public double calculateResult() {
        double secondOperand = getDisplayedTextAsDouble();
        return this.operator.evaluate(firstOperand, secondOperand);
    }

    public String getDisplayedText() {
        return displayedText;
    }
}
