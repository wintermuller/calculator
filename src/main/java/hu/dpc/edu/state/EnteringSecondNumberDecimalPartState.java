package hu.dpc.edu.state;

import hu.dpc.edu.Calculator;

public class EnteringSecondNumberDecimalPartState extends EnteringSecondNumberState {

    @Override
    public void decimalSeparatorPressed(Calculator calculator) {
        //do nothing
    }

    @Override
    public String toString() {
        return "Entering second number decimal part";
    }
}
