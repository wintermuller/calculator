package hu.dpc.edu.state;

import hu.dpc.edu.Calculator;
import hu.dpc.edu.CalculatorState;
import hu.dpc.edu.Operator;

public class EnteringSecondNumberState extends CalculatorState {

    @Override
    public void onEnter(Calculator calculator) {
        //do nothing
    }

    @Override
    public void operatorPressed(Calculator calculator, Operator operator) {
        Double result = calculator.calculateResult();
        calculator.setOperator(operator);
        calculator.setDisplayedText(result.toString());
        calculator.setState(new EnteringSecondNumberInitialState());
    }

    @Override
    public void numberButtonPressed(Calculator calculator, String num) {
        calculator.appendToDisplayedText(num);
    }

    @Override
    public void calculateButtonPressed(Calculator calculator) {
        Double result = calculator.calculateResult();
        calculator.setDisplayedText(result.toString());
        calculator.setState(new DisplayResultState());
    }

    @Override
    public void decimalSeparatorPressed(Calculator calculator) {
        calculator.appendToDisplayedText(".");
        calculator.setState(new EnteringSecondNumberDecimalPartState());
    }

    @Override
    public String toString() {
        return "Entering second number";
    }
}
