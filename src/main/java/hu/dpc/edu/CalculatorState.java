package hu.dpc.edu;

public abstract class CalculatorState {

    public void addButtonPressed(Calculator calculator) {
        operatorPressed(calculator, (op1, op2) -> op1 + op2);
    }

    public void subtractButtonPressed(Calculator calculator) {
        operatorPressed(calculator, (op1, op2) -> op1 - op2);
    }

    public void multiplyButtonPressed(Calculator calculator) {
        operatorPressed(calculator, (op1, op2) -> op1 * op2);
    }

    public void divisionButtonPressed(Calculator calculator) {
        operatorPressed(calculator, (op1, op2) -> op1 / op2);
    }

    public abstract void operatorPressed(Calculator calculator, Operator operator);

    protected abstract void numberButtonPressed(Calculator calculator, String number);

    public void numberButtonPressed(Calculator calculator, int num) {
        numberButtonPressed(calculator, String.valueOf(num));
    }

    public abstract void calculateButtonPressed(Calculator calculator);

    public abstract void decimalSeparatorPressed(Calculator calculator);

    public void onLeave(Calculator calculator) {
    }

    public void onEnter(Calculator calculator) {
    }

}
